BlackStone Studio Challenge

### Julio César Sanjuan.

##Create a React Component:
#Write a component that rotates over a list of images when a user hovers on the element.
#When a the mouse is over the element it should rotate between the images & stop once the mouse is not over and go back to the first image.
#The component will receive props: images (array of images), name (String) & description (String)
#The component should look like a card (https://bit.ly/2Zubi3r) component.
***Bonus points: The images are preloaded before starting to rotate over images & has nice transitions.***


