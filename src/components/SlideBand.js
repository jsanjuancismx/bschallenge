import React from 'react';
import ReactDOM from 'react-dom';
import CarruselCard from "./CarruselCard";

function scrollTo(element, to, widget, listener) {
	if(typeof widget !== "undefined"){
		if(widget.state.isFocused===true && element.scrollTop < to){
			element.scrollTop = element.scrollTop + 1;
			setTimeout(scrollTo, 20, element, to, widget, listener);
		} 
		if(!(element.scrollTop < to)) if(typeof listener == "function") listener(widget);
	}
}

function scrollToTop(element, to, widget, listener) {
	if(typeof widget !== "undefined"){
		if(element.scrollTop > 0){
			element.scrollTop = element.scrollTop - 10;
			setTimeout(scrollToTop, 1, element, to, widget, listener);
		} else if(typeof listener == "function")	listener(widget);
	}
}
class SlideBand extends React.Component{
	constructor(props){
		super(props);
		this.state = {isFocused:false, canSlide:false};
		this.domnode = null;
	}
	componentDidMount(){
		this.domnode = ReactDOM.findDOMNode(this);
		var randomTop = Math.floor(Math.random() * this.domnode.scrollHeight);
		this.domnode.scrollTop = randomTop;
		this.setState({canSlide:true});
	}
	
	playSlide(evt){
		if(this.state.canSlide === true){
			var thebottom = Math.floor(this.domnode.scrollHeight) - this.domnode.clientHeight -1;
			this.setState({isFocused:true});
			setTimeout(scrollTo, 100, this.domnode, thebottom, this, (target)=>{
				setTimeout(()=>{
					scrollToTop(target.domnode, 0, target, (target)=>{
						if(target.state.isFocused)
							target.playSlide();
					});
				},1000);
			});
		}
	}
	stopSlide(evt){
		this.setState({isFocused:false});
	}
	createCards(){
		let cards = [];
		for(var card of this.props.cards){
			cards.push(
				<CarruselCard 
					image={card.picture}
					title={card.name} 
					details={card.description} 
				/>
			);
		}
		return cards;
	}
	render(){
		return(
			<div class="carrusel-band" onMouseEnter={this.playSlide.bind(this)} onMouseLeave={this.stopSlide.bind(this)}>
				{this.createCards()}
			</div>
		);
	}
}
export default SlideBand;