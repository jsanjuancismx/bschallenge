import React from 'react';
import ReactDOM from 'react-dom';
class CarruselCard extends React.Component{
	loadedImage(_this){
		var thedom= ReactDOM.findDOMNode(_this);
		if(thedom){
			//thedom.children[1].style.display= "none";
		}
	}
	render(){
		return(
			<div class="carrusel-card">
				<img src={process.env.PUBLIC_URL +'/assets/images/'+ this.props.image} alt={this.props.title+" picture."} class="carrusel-img" onLoad={this.loadedImage(this)} />
				<h5>{this.props.title}</h5>
				<p>{this.props.details}</p>
			</div>
		);
	}
}
export default CarruselCard;