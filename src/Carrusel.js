import React from 'react';
import "./styles/carrusel.css";
import SlideBand from "./components/SlideBand";

class Carrusel extends React.Component{
	constructor(props){
		super(props);
	}
	componentDidMount(){
	}
	componentWillUnmount(){
	}
	render(){
		return(
			<div class="carrusel">
				<SlideBand  cards={this.props.cards.slice(0,10)}/>
				<SlideBand  cards={this.props.cards.slice(10,20)}/>
				<SlideBand  cards={this.props.cards.slice(5,15)}/>
				<SlideBand  cards={this.props.cards.slice(7,17)}/>
			</div>
		);
	}
}

export default Carrusel;