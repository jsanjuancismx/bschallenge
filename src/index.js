import React from 'react';
import ReactDOM from 'react-dom';
import Carrusel from './Carrusel';
import * as serviceWorker from './serviceWorker';
var cities = [
	{	name:"Amsterdam",
		description:"Amsterdam es la capital de los Países Bajos, conocida por su patrimonio artístico, su elaborado sistema de canales y sus casas angostas con fachadas de dos aguas, herencias de la Edad de Oro de la ciudad durante el siglo XVII",
		picture:"amsterdam.jpeg"
	},
	{	name:"Barcelona",
		description:"la capital cosmopolita de la región de Cataluña en España, es conocida por su arte y arquitectura. La fantástica iglesia de la Sagrada Familia y otros hitos modernistas diseñados por Antoni Gaudí adornan la ciudad",
		picture:"barcelona.jpg"
	},
	{	name:"Berlin",
		description:"capital alemana, data del siglo XIII. Los elementos que recuerdan la turbulenta historia de la ciudad en el siglo XX incluyen el Monumento del Holocausto y los restos del Muro de Berlín con grafitis",
		picture:"berlin.jpg"
	},
	{	name:"Bora bora",
		description:"es una pequeña isla del Pacífico Sur al noroeste de Tahití en la Polinesia Francesa. Rodeada de motus (islotes) con orillas de arena y una laguna turquesa protegida por un arrecife de coral, es conocida por el buceo",
		picture:"boro-bora.jpg"
	},
	{	name:"Ciudad de México",
		description:"es la densamente poblada capital de México que se encuentra a gran altura. Es famosa por su Templo Mayor (un templo azteca del siglo XIII), la Catedral Metropolitana de México, de estilo barroco, de los conquistadores españoles y el Palacio Nacional, que alberga murales históricos de Diego Rivera",
		picture:"cdmx.jpg"
	},
	{	name:"Estambul",
		description:"es una ciudad importante en Turquía, que se ubica en Europa y Asia a lo largo del estrecho de Bósforo. La Ciudad Antigua refleja las influencias culturales de los distintos imperios que gobernaron la región",
		picture:"estambul.jpg"
	},
	{	name:"Estocolmo",
		description:"la capital de Suecia, abarca 14 islas y más de 50 puentes en un extenso archipiélago del mar Báltico. Las calles de adoquines y los edificios color ocre de Gamla Stan (la ciudad vieja) albergan la catedral de San Nicolás de Estocolmo del siglo XIII, el palacio real Kungliga Slottet y el Museo Nobel, que se enfoca en el premio del mismo nombre",
		picture:"estocolmo.jpg"
	},
	{	name:"Guadalajara",
		description:"es una ciudad en el oeste de México. Es conocida por el tequila y la música mariachi, ambos nacidos en Jalisco, el estado del que Guadalajara es la capital.",
		picture:"guadalajara.jpg"
	},
	{	name:"Helsinki",
		description:"la capital de Finlandia en el sur del país, se ubica en una península del golfo de Finlandia. Su avenida central, Mannerheimintie, está bordeada de instituciones como el Museo Nacional, que recorre la historia de Finlandia desde la Edad de Piedra hasta la actualidad.",
		picture:"helsinki.jpg"
	},
	{	name:"Kuala Lumpur",
		description:"Kuala Lumpur es la capital de Malasia. En su moderno perfil, se destacan las Torres Petronas de 451 m de altura, un par de rascacielos cubiertos de vidrio y acero con diseños islámicos.",
		picture:"kuala-lumpur.jpg"
	},
	{	name:"Machu picchu",
		description:"es una ciudadela inca ubicada en las alturas de las montañas de los Andes en Perú, sobre el valle del río Urubamba",
		picture:"machu-picchu.jpg"
	},
	{	name:"Marruecos",
		description:"es un país de África del Norte que limita con el océano Atlántico y el mar Mediterráneo, y se distingue por las influencias culturales bereberes, árabes y europeas",
		picture:"marruecos.jpg"
	},
	{	name:"Nairobi",
		description:"es la capital de Kenia. Además de su centro urbano, la ciudad tiene el Parque Nacional de Nairobi, una gran reserva de caza conocida por criar rinocerontes negros en peligro de extinción y albergar jirafas, cebras y leones",
		picture:"nairobi.jpeg"
	},
	{	name:"Nueva York",
		description:"incluye 5 distritos que se ubican donde el río Hudson desemboca en el océano Atlántico. En su centro se encuentra Manhattan, un distrito densamente poblado que se encuentra entre los principales centros comerciales, financieros y culturales del mundo",
		picture:"nyc.jpeg"
	},
	{	name:"Paris",
		description:"la capital de Francia, es una importante ciudad europea y un centro mundial del arte, la moda, la gastronomía y la cultura. Su paisaje urbano del siglo XIX está entrecruzado por amplios bulevares y el río Sena",
		picture:"paris.jpg"
	},
	{	name:"Praga",
		description:"la capital de la República Checa, está dividida por el río Moldava. Recibe el apodo de la “Ciudad de las Cien Torres” y es conocida por la Plaza de la Ciudad Vieja,",
		picture:"praga.jpg"
	},
	{	name:"Río de Janeiro",
		description:"es una enorme ciudad costera de Brasil, famosa por sus playas de Copacabana e Ipanema, la estatua del Cristo Redentor de 38 m de alto sobre el cerro del Corcovado y el morro ",
		picture:"rio.jpg"
	},
	{	name:"Roma",
		description:"la capital de Italia, es una extensa ciudad cosmopolita que tiene a la vista casi 3,000 años de arte, arquitectura y cultura de influencia mundial.",
		picture:"roma.jpg"
	},
	{	name:"Tokyo",
		description:"la ajetreada capital de Japón, mezcla lo ultramoderno y lo tradicional, desde los rascacielos iluminados con neones hasta los templos históricos",
		picture:"tokyo.jpg"
	},
	{	name:"Venecia",
		description:"la capital de la región de Véneto en el norte de Italia, abarca más de 100 islas pequeñas en una laguna del mar Adriático. No tiene caminos, sino solo canales, incluida la vía pública del Gran Canal, bordeada de palacios renacentistas y góticos",
		picture:"venecia.jpg"
	}
];

ReactDOM.render(<Carrusel cards={cities} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
